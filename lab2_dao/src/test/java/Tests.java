import com.company.model.*;
import com.company.dao.*;

import org.junit.Before;
import org.junit.Test;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.List;

import static junit.framework.TestCase.*;

public class Tests {
    Connection con;
    DAO dao;

    @Before
    public void init() throws SQLException {
        // tests only with real db
        con = DriverManager.getConnection("jdbc:postgresql://localhost:5432/test",
                "admin", "admin");
        dao = new DAO(con);
    }

    @Test
    public void getAllUsers_GetFromDB_ValidUsersDate() throws SQLException {
        List<User> users = dao.getAllUsers();

        assertEquals(3, users.size());
        assertEquals("user1", users.get(0).getName());
        assertEquals("user2", users.get(1).getName());
    }
    @Test
    public void getUser_GetFromDB_ValidUserDate() throws SQLException {
        int id = 3;
        User user = dao.getUser(id);

        assertNotNull(user);
        assertEquals(id, user.getId());
        assertEquals("user3", user.getName());
        assertEquals(19, user.getAge());
    }

    @Test
    public void getAllFilms_GetFromDB_ValidFilmsDate() throws SQLException {
        List<Film> films = dao.getAllFilms();

        assertEquals(2, films.size());
        assertEquals("Some film 3", films.get(0).getName());
        assertEquals("Some film 2", films.get(1).getName());
    }
    @Test
    public void getFilm_GetFromDB_ValidFilmDate() throws SQLException {
        int id = 1;
        Film film = dao.getFilm(id);

        assertNotNull(film);
        assertEquals(id, film.getId());
        assertEquals("Some film 3", film.getName());
        assertEquals("Universal", film.getCompany());
    }

    @Test
    public void getAllSerials_GetFromDB_ValidSerialsDate() throws SQLException {
        List<Serial> serials = dao.getAllSerials();

        assertEquals(2, serials.size());
        assertEquals("Some serial. vol.1", serials.get(0).getName());
        assertEquals("Some serial. vol.2", serials.get(1).getName());
    }
    @Test
    public void getSerial_GetFromDB_ValidSerialDate() throws SQLException {
        int id = 4;
        Serial serial = dao.getSerial(id);

        assertNotNull(serial);
        assertEquals(id, serial.getId());
        assertEquals("Some serial. vol.2", serial.getName());
        assertEquals(24, serial.getNumber_of_series());
    }

}
