package com.company;

import com.company.dao.DAO;
import java.sql.*;

import java.util.List;
import com.company.model.User;
import com.company.model.Film;
import com.company.model.Serial;

public class Main {
    public static void main(String[] args) {
        try {
            Connection con = DriverManager.getConnection("jdbc:postgresql://localhost:5432/PogorelovLabs",
                    "admin", "admin");
            DAO dao = new DAO(con);

            System.out.println("Users: ");
            List<User> users = dao.getAllUsers();
            for(User user: users){
                user.PrintUser();
            }

            System.out.println("Films: ");
            List <Film> films = dao.getAllFilms();
            for(Film film: films){
                film.PrintFilm();
            }

            System.out.println("Serials: ");
            List <Serial> serials = dao.getAllSerials();
            for(Serial serial: serials){
                serial.PrintSerial();
            }

            System.out.println("\nGet By Index: ");
            User user = dao.getUser(1);
            user.PrintUser();

            Film film = dao.getFilm(1);
            film.PrintFilm();

            Serial serial = dao.getSerial(3);
            serial.PrintSerial();

        } catch (SQLException e){
            e.printStackTrace();
        }
    }
}
