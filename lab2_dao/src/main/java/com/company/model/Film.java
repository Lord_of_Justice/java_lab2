package com.company.model;

@TableName(name = "videos")
@DiscriminatorValue(value ="film")
public class Film extends Video{
    private String company;

    public Film(){};
    public Film(int id, String name, long duration, boolean subtitles, double aspect_ratio, String company){
        super(id, name, duration, subtitles, aspect_ratio);
        this.company = company;
    }
    public String getCompany() {
        return this.company;
    }
    public void setCompany(String company) {
        this.company = company;
    }

    public void PrintFilm(){
        System.out.println(InfoAboutVideo() + " company: " + company);
    }
}
