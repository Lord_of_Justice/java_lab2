package com.company.model;

import java.lang.annotation.*;

@Retention(RetentionPolicy.RUNTIME)
@Inherited
public @interface DiscriminatorColumn {
    String name();
}
