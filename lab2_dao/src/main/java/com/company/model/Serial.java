package com.company.model;

@TableName(name = "videos")
@DiscriminatorValue(value ="serial")
public class Serial extends Video {
    private int number_of_series;

    public Serial(){};
    public Serial(int id, String name, long duration, boolean subtitles, double aspect_ratio, int number_of_series){
        super(id, name, duration, subtitles, aspect_ratio);
        this.number_of_series = number_of_series;
    }
    public int getNumber_of_series() {
        return number_of_series;
    }
    public void setNumber_of_series(int number_of_series) {
        this.number_of_series = number_of_series;
    }

    public void PrintSerial(){
        System.out.println(InfoAboutVideo() + " number of series: " + number_of_series);
    }
}
