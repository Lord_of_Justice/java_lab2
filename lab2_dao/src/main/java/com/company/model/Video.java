package com.company.model;

@DiscriminatorColumn(name = "video_type")
public abstract class Video {
    private int id;
    private String name;
    private long duration;
    private boolean subtitles;
    private double aspect_ratio; //Співвідношення сторін екрану

    public Video(){ }
    public Video(int id, String name, long duration, boolean subtitles, double aspect_ratio){
        this.id = id;
        this.name = name;
        this.duration = duration;
        this.subtitles = subtitles;
        this.aspect_ratio = aspect_ratio;
    }

    public void setId(int id){
        this.id = id;
    }
    public int getId(){
        return this.id;
    }
    public void setName(String name){
        this.name = name;
    }
    public String getName(){
        return this.name;
    }
    public void setDuration(long duration){
        this.duration = duration;
    }
    public long getDuration(){
        return this.duration;
    }
    public void setSubtitles(boolean subtitles){
        this.subtitles = subtitles;
    }
    public boolean getSubtitles(){
        return this.subtitles;
    }
    public void setAspect_ratio(double aspect_ratio){
        this.aspect_ratio = aspect_ratio;
    }
    public double getAspect_ratio(){
        return this.aspect_ratio;
    }

    protected String InfoAboutVideo(){
        String subtitlesOn = subtitles ? "is": "none";
        return String.format("ID: %d - \'%s\', duration: %d, subtitles: %s, aspect ratio: %f",
                id, name, duration, subtitlesOn, aspect_ratio);
    }

}
