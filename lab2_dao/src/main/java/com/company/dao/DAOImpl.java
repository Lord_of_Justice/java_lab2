package com.company.dao;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.sql.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.company.model.*;

public class DAOImpl<T> implements IDAOImpl<T> {
    Class<T> clazz;
    private Connection connection;
    DiscriminatorColumn columnAnnotation;
    String discriminatorColumn;
    DiscriminatorValue valueAnnotation;
    String discriminatorValue;

    public DAOImpl(Class<T> clazz, Connection connection){
        this.clazz = clazz;
        this.connection = connection;
        columnAnnotation = clazz.getAnnotation(DiscriminatorColumn.class);
        discriminatorColumn = columnAnnotation != null ? columnAnnotation.name() : null;
        valueAnnotation = clazz.getAnnotation(DiscriminatorValue.class);
        discriminatorValue = valueAnnotation != null ? valueAnnotation.value() : null;
    }

    public T getEntity(int id) throws SQLException{
        String tableName = clazz.getAnnotation(TableName.class).name();

        String query = String.format("SELECT * FROM %s WHERE id = %d", tableName, id);
        if (discriminatorColumn != null) {
            query = query.concat(String.format(" AND %s = \'%s\'", discriminatorColumn, discriminatorValue));
        }
        //System.out.println(query);
        Statement st = connection.createStatement();
        ResultSet rs = st.executeQuery(query);
        T entity = rs.next() ? createEntity(rs) : null;

        rs.close();
        st.close();
        return entity;
    }
    public List<T> getEntityList() throws SQLException{
        Statement st = connection.createStatement();
        ResultSet resultSet = st.executeQuery("select * from " + clazz.getAnnotation(TableName.class).name());

        List<T> entities = new ArrayList<>();
        while(resultSet.next()) {
            if (discriminatorColumn == null || discriminatorValue.equals(resultSet.getString(discriminatorColumn))){
                entities.add(createEntity(resultSet));
            }
        }
        resultSet.close();
        st.close();
        return entities;
    }

    public T createEntity(ResultSet rs){
        T entity;
        try {
            entity = clazz.getConstructor().newInstance();

            List<Field> fields = new ArrayList<>();
            if (clazz.getSuperclass() != null){
                fields.addAll(Arrays.asList(clazz.getSuperclass().getDeclaredFields()));
            }
            fields.addAll(Arrays.asList(clazz.getDeclaredFields()));

            for(Field f: fields){
                f.setAccessible(true);
                try{
                    Object value = rs.getObject(f.getName());
                    f.set(entity, value);
                } catch (SQLException e){
                    e.printStackTrace();
                }
            }
        } catch (IllegalAccessException
                | InvocationTargetException
                | NoSuchMethodException
                | InstantiationException e){
            e.printStackTrace();
            entity = null;
        }
        return entity;
    }

}