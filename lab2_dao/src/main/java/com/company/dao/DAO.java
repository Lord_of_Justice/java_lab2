package com.company.dao;

import com.company.model.Film;
import com.company.model.Serial;
import com.company.model.User;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.List;

public class DAO {
    DAOImpl<User> userDAO;
    DAOImpl<Film> filmDAO;
    DAOImpl<Serial> serialDAO;
    Connection connection;

    public DAO(Connection conection) throws SQLException{
        this.connection = conection;
        userDAO = new DAOImpl<User>(User.class, connection);
        filmDAO = new DAOImpl<Film>(Film.class, connection);
        serialDAO = new DAOImpl<Serial>(Serial.class, connection);
    }

    public User getUser(int id) throws SQLException{
        return userDAO.getEntity(id);
    }
    public List<User> getAllUsers() throws SQLException{
        return userDAO.getEntityList();
    }
    public Film getFilm(int id) throws SQLException{
        return filmDAO.getEntity(id);
    }
    public List<Film> getAllFilms() throws SQLException{
        return filmDAO.getEntityList();
    }
    public Serial getSerial(int id) throws SQLException{
        return serialDAO.getEntity(id);
    }
    public List<Serial> getAllSerials() throws SQLException{
        return serialDAO.getEntityList();
    }
}
